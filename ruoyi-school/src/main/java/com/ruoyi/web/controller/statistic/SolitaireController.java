package com.ruoyi.web.controller.statistic;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 接龙统计
 *
 * @author mengfanchao
 */
@Controller
@RequestMapping("/school/solitaire")
public class SolitaireController  extends BaseController {

    private String prefix = "school/solitaire";

    @Autowired
    private ISysUserService userService;


    @RequiresPermissions("school:solitaire:view")
    @GetMapping()
    public String user()
    {
        return prefix + "/solitaire";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUser user)
    {
        String jielong = user.getLoginName();
        user.setLoginName(null);
        List<SysUser> list = userService.selectUserList(user);
        List<SysUser> notInJielonglist = new ArrayList<>();
        if(list != null && list.size() > 0){
            for(SysUser u : list){
                if(!jielong.contains(u.getUserName())){
                    notInJielonglist.add(u);
                }
            }
        }
        return getDataTable(notInJielonglist);
    }
}
