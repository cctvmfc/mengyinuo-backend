package com.ruoyi.android.controller;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * app端用户管理控制类
 */
@RestController
@RequestMapping("/api/android/user/")
public class ApiUserController extends BaseController {

    @Autowired
    private ISysUserService userService;


    /**
     * app用户登陆
     *
     * @param parameter
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public JSONObject login(@RequestBody Map<String, Object> parameter, HttpServletResponse resp) throws Exception {
        UsernamePasswordToken token = null;
        //用户名
        String userName = (String) parameter.get("userName");
        //判断用户名
        if (StringUtils.isEmpty(userName)) {
            return ajaxResultToJSONObject(error("用户名"));
        }
        //密码
        String password = (String) parameter.get("password");
        //判断密码
        if (StringUtils.isEmpty(password)) {
            return ajaxResultToJSONObject(error("密码"));
        }
        //查询用户信息
        SysUser user = userService.selectUserByLoginName(userName);
        //判断用户是否存在
        if (user == null) {
            return ajaxResultToJSONObject(error("用户不存在"));
        }
//        try {
//            Subject subject = ShiroUtils.getSubject();
//            token = new UsernamePasswordToken(userName, password);
//            subject.login(token);
//            user.setPassword("");
//            user.setSalt("");
//            Cookie cookie = new Cookie("JSESSIONID", ShiroUtils.getSession().getId().toString());
//            cookie.setMaxAge(30 * 24 * 60 * 60);
//            resp.addCookie(cookie);
//            JSONObject data = (JSONObject) JSONObject.toJSON(user);
//            return ajaxResultToJSONObject(success("登录成功", data));
//        } catch (IOException e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.LOGIN_FAILED);
//        } catch (UnknownAccountException e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.LOGIN_FAILED, e.getMessage() + ",");
//        } catch (IncorrectCredentialsException e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.USERNAME_PASSWORD_ERROR);
//        } catch (LockedAccountException e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.USERLOCKING);
//        } catch (AuthenticationException e) {
//            return ResultInfo.error(ResultInfo.LOGIN_FAILED);
//        }
        return null;
    }
//
//    /**
//     * app用户退出
//     *
//     * @return
//     */
//    @RequestMapping(value = "logout", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
//    public JSONObject logout() {
//        try {
//            SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
//            //验证是否登陆
//            if (StringUtils.isEmpty(sysUserEntity)) {
//                return ResultInfo.error(ResultInfo.NO_LOGIN);
//            }
//            ShiroUtils.logout();
//            return ResultInfo.success();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.LOGIN_OUT_ERROR);
//        }
//    }
//
//    /**
//     * app修改密码
//     *
//     * @param parameter
//     * @return
//     */
//    @RequestMapping(value = "modifyPassword", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
//    public JSONObject modifyPassword(@RequestBody Map<String, Object> parameter) {
//        try {
//            SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
//            //验证是否登陆
//            if (StringUtils.isEmpty(sysUserEntity)) {
//                return ResultInfo.error(ResultInfo.NO_LOGIN);
//            }
//            String oldPassword = (String) parameter.get("oldPassword");
//            //验证旧密码是否为空
//            if (StringUtils.isEmpty(oldPassword)) {
//                return ResultInfo.error(ResultInfo.ISEMPTY, "原密码");
//            }
//            String newPassword = (String) parameter.get("newPassword");
//            //验证新密码是否为空
//            if (StringUtils.isEmpty(newPassword)) {
//                return ResultInfo.error(ResultInfo.ISEMPTY, "新密码");
//            }
//            //原密码
//            oldPassword = ShiroUtils.sha256(oldPassword, sysUserEntity.getSalt());
//            //验证原密码
//            if (!sysUserEntity.getPassword().equals(oldPassword)) {
//                return ResultInfo.error(ResultInfo.OLD_PASSWORD_ERROR);
//            }
//            //新密码
//            newPassword = ShiroUtils.sha256(newPassword, sysUserEntity.getSalt());
//            sysUserService.updatePassword(sysUserEntity.getUserId(), oldPassword, newPassword);
//            ShiroUtils.logout();
//            return ResultInfo.success();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.MODIFY_PASSWORD_ERROR);
//        }
//    }
//
//
//    @PostMapping(path = "/lgLogout", consumes = "application/json")
//    @ResponseBody
//    public JSONObject lgLogout(@RequestBody Map<String, Object> params) {
//        try {
//            if (StringUtils.isEmpty(params.get("token"))) {
//                return ResultInfo.error(ResultInfo.BAD_REQUEST);
//            }
//            if (tokenExpire <= 0) {
//                return ResultInfo.success();
//            }
//            // 删除 token
//            String key = Constant.TOKEN_PREFIX + params.get("token").toString();
//            stringRedisTemplate.delete(key);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.ERROR);
//        }
//        return ResultInfo.success();
//    }
//
//    /**
//     * 生成token
//     *
//     * @param lgLgxxEntity
//     * @return
//     */
//    private String genToken(LgLgxxEntity lgLgxxEntity) {
//        String token = GenUtils.createId();
//        if (tokenExpire <= 0) {
//            return token;
//        }
//        // 放入缓存并设置过期时间
//        String key = Constant.TOKEN_PREFIX + token;
//        stringRedisTemplate.opsForValue().set(key, lgLgxxEntity.getLgbm());
//        stringRedisTemplate.expire(key, tokenExpire, TimeUnit.MINUTES);
//        return token;
//    }
}
