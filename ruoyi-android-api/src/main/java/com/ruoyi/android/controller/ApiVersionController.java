package com.ruoyi.android.controller;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ApiVersionController  extends BaseController {

//    @Autowired
//    private SysAppversionService sysAppversionService;
//    @Autowired
//    private LgAppVersionService lgAppVersionService;
//    @Value("${security.app.downloadUrl}")
//    private String downloadUrl;
//    @Value("${spring.uploadFilePath}")
//    private String path;

    /**
     * 下载移动警务检查app
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "downloadApk", method = RequestMethod.GET)
    public void downloadApk(HttpServletRequest request, HttpServletResponse response) {
//        SysAppversionVO sysAppversionVO = sysAppversionService.findVersion();
//        download(request, response, sysAppversionVO);
    }

    /**
     * 下载一体机APP
     *
     * @param request
     * @param response
     */
    @GetMapping("/getApp")
    public void getApp(HttpServletRequest request, HttpServletResponse response) {
//        Map<String, Object> param = new HashMap<>();
//        param.put("apkCode", "2");
//        SysAppversionVO sysAppversionVO = sysAppversionService.getVersion(param);
//        download(request, response, sysAppversionVO);
    }

//    private void download(HttpServletRequest request, HttpServletResponse response, SysAppversionVO sysAppversionVO) {
//        InputStream is = null;
//        OutputStream os = null;
//        try {
//            //文件名
//            String fileName = sysAppversionVO.getVersionName() + sysAppversionVO.getVersionCode() + ".apk";
//            //解决乱码
//            String agent = request.getHeader("USER-AGENT");
//            if (null != agent) {
//                if (-1 != agent.indexOf("Firefox")) {//Firefox
//                    fileName = "=?UTF-8?B?" + (new String(org.apache.commons.codec.binary.Base64.encodeBase64(fileName.getBytes("UTF-8")))) + "?=";
//                } else if (-1 != agent.indexOf("Chrome")) {//Chrome
//                    fileName = new String(fileName.getBytes(), "ISO8859-1");
//                } else {//IE7+
//                    fileName = java.net.URLEncoder.encode(fileName, "UTF-8");
//                    fileName = StringUtils.replace(fileName, "+", "%20");//替换空格
//                }
//            }
//            // 设置response的编码方式
//            response.setContentType("application/x-msdownload");
//            // 设置附加文件名
//            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
//            File file = new File(path + sysAppversionVO.getFilePath());
//            is = new FileInputStream(file);
//            os = response.getOutputStream();
//            byte[] b = new byte[2048];
//            int length;
//            while ((length = is.read(b)) > 0) {
//                os.write(b, 0, length);
//            }
//            response.setContentLength((int) file.length());
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            IOUtil.close(os, is);
//        }
//    }

    @PostMapping("/getVersion")
    @ResponseBody
    public JSONObject getVersion(@RequestBody Map<String, Object> param) {
//        try {
//            if (StringUtils.isEmpty(param.get("lgbm")) || StringUtils.isEmpty(param.get("version")) || StringUtils.isEmpty("apkCode")) {
//                return ResultInfo.error(ResultInfo.BAD_REQUEST);
//            }
//            // 更新现有版本
//            lgAppVersionService.updateVersion(param);
//            // 返回现有版本
//            SysAppversionVO sysAppversionVO = sysAppversionService.getVersion(param);
//            sysAppversionVO.setFilePath(downloadUrl);
//            return ResultInfo.success((JSONObject) JSONObject.toJSON(sysAppversionVO));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResultInfo.error(ResultInfo.ERROR);
//        }
        return null;
    }
}
